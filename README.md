# Coreomedia 
istruzioni per l'uso e altre info utili

## Come usare tracker senza visore
https://visualprogramming.net/blog/2018/using-htc-vive-trackers-without-headset/

Activate the ’null’ driver, find this file on your drive:

`SteamDirectory\steamapps\common\SteamVR\drivers\null\resources\settings\default.vrsettings`

change `enable` to `true`.

Then open this file:
`SteamDirectory\config\steamvr.vrsettings`

Add the following entries to the "steamvr" section: 
```json
"forcedDriver": "null", 
"activateMultipleDrivers": "true",
```

SteamDirectory is usually C:\Program Files (x86)\Steam.

Also make sure to disable the "SteamVR Home" on startup. Otherwise it will try to render into the null HMD and consume 100% of one CPU core: If SteamVR was running, close and restart it.

## VVVV beta 40__
aprire patch nella cartella con lo script `_OPEN ME.bat`

connettere con max:
1. check ip su vvvv
2. Selezionare l'ip locale della macchina, quello che inizia con 192.168. etc etc
3. Questo ip va inserito su max, nell'input sopra ethernet in dance the distance presentation
4. Allo stesso modo serve controllare ip sul mac
5. Aprire il terminal, digitare il comando

    `ifconfig | grep 192.*`
    
6. cercare l'ip al parametro en0
7. usare il valore inet 192.168. ecc ecc come remote host su vvvv
 
## Timeline TILDA
After you clone or download a repo on it's first start you will most likely see that the Renderer (Tilda) node is red. It is because of missing Elementa dependency.
In order to fix that you need to open Renderer (Tilda) by right click. Left click on gray square in the top left corner -> All Documents -> VVVV.Tilda.GUI.vl (should be red) -> Dependences -> right click on Elementa (also red) -> Install
After that it should work fine. Maybe you need to restart vvvv

## Bluetooth not available SteamVR

Ran into this Bluetooth issue in SteamVR with my Vive Pro 2 where I couldn't change my Base Station 2.0 channels and couldn't update their firmware. I lucked out and found a solution for this that fixed the issue and got Bluetooth working in SteamVR (I was subsequently able to change my Base Station 2.0 channels and update their firmware). Apparently some of the SteamVR/linkbox drivers are not compliant with Windows 11's "Memory integrity" setting and Windows blocks those drivers from installing, which completely prevents you from being able to enable Bluetooth in SteamVR. To fix:

Open Windows Settings, go to Privacy and Security->Windows Security->Device Security->Core Isolation Details and turn "Memory integrity" off. You will need to restart your PC for this setting to take effect. After it is finished rebooting, start up SteamVR and it should automatically install the missing drivers. Then you should be able to enable Bluetooth and make the necessary setting changes and driver updates!

I can't seem to locate the original place I found this information to give them credit, so if anyone else can find that information please post it because this person deserves massive credit!