
StructuredBuffer<float3> sbPosition;
StructuredBuffer<float3> sbPosition2;
StructuredBuffer<float4> sbColor;
StructuredBuffer<float4> sbColor2;


struct PosColor
{
	float3 p;
	float4 c;
};

cbuffer cbParams : register(b0)
{
	int elementcount;
	float mix = 0.0f;
};


RWStructuredBuffer<PosColor> RWVertexBuffer : BACKBUFFER;

[numthreads(64, 1,1)]
void CS(uint3 i : SV_DispatchThreadID)
{
	//Safeguard if we don't use a multiple
	if (i.x >= elementcount) { return; }

	PosColor pc;
	pc.p = lerp(sbPosition[i.x], sbPosition2[i.x], mix);
	pc.c = lerp(sbColor[i.x], sbColor2[i.x], mix);

	RWVertexBuffer[i.x] = pc;
		


}

technique11 Process
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS() ) );
	}
}






