
RWStructuredBuffer<float3> Output : BACKBUFFER;

StructuredBuffer<float3> points;
int pointSize;
int resampleSize;

float map(float val,float x, float y, float a, float b) {
	return (b-a)*(val-x)/(y-x)+a;
}

[numthreads(64, 1, 1)]
void Resample_Linear( uint3 i : SV_DispatchThreadID)
{ 
	float t = float(i.x) / float(resampleSize) * float(pointSize-1);
	
	// this is a constant so maybe should be calculated on CPU, outside the shader
	float tMax = float((resampleSize-1)) / float(resampleSize) * float(pointSize-1);
	
	t = map(t, 0.0, tMax, 0.0, float(pointSize-1));
	float tI = int(t);
	
	Output[i.x] = lerp(points[tI], points[tI+1], frac(t)+((tI+1)==t));
}

[numthreads(64, 1, 1)]
void Resample_Point( uint3 i : SV_DispatchThreadID)
{ 
	float t = float(i.x) / float(resampleSize) * float(pointSize-1);
	
	// this is a constant so maybe should be calculated on CPU, outside the shader
	float tMax = float((resampleSize-1)) / float(resampleSize) * float(pointSize-1);
	
	t = map(t, 0.0, tMax, 0.0, float(pointSize-1));
	float tI = int(t);
	
	Output[i.x] = points[tI];
}

technique11 Linear
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, Resample_Linear() ) );
	}
}

technique11 Point
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, Resample_Point() ) );
	}
}

