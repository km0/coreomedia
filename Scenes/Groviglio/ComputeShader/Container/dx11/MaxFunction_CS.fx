//@author: Noir
//@tags: clamp function



RWStructuredBuffer<float3> output : BACKBUFFER;

StructuredBuffer<float3> bufferIN;
uint threadCount;

float ClipPlan = 0.0;
float3 Box;
float2 MinMax = float2(-1.0,1.0);

[numthreads(128,1,1)]
void CS_maxX( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];
	output[dtid.x].x = max(output[dtid.x].x, ClipPlan);	
	
}

[numthreads(128,1,1)]
void CS_maxY( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];
	output[dtid.x].y = max(output[dtid.x].y, ClipPlan);	
	
}

[numthreads(128,1,1)]
void CS_maxZ( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];
	output[dtid.x].z = max(output[dtid.x].z, ClipPlan);	
	
}

[numthreads(128,1,1)]
void CS_bBox( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];

	
		if(output[dtid.x].y > Box.y)
			{output[dtid.x].y = Box.y;
			//Output[DTid.x].vel.y =	-0.01f * noise[DTid.x].x;
			}
		
	    else if(output[dtid.x].y < -Box.y)
			{output[dtid.x].y = -Box.y;
				//Output[DTid.x].vel.y =	0.01*noise[DTid.x].x;
			}
		
	    if(output[dtid.x].x > Box.x) 
			{ output[dtid.x].x = Box.x;
				//Output[DTid.x].vel.x =	-0.01*noise[DTid.x].x;
			}
		
	    else if(output[dtid.x].x < -Box.x) 
			{output[dtid.x].x= -Box.x;
				//Output[DTid.x].vel.x =	0.01*noise[DTid.x].x;
			}
		
	    if(output[dtid.x].z > Box.z) 
			{output[dtid.x].z = Box.z;
			//Output[DTid.x].vel.z =	-0.01*noise[DTid.x].x;
			}
		
		else if(output[dtid.x].z < -Box.z)
			{output[dtid.x].z =-Box.z;
				//Output[DTid.x].vel.z =	0.01*noise[DTid.x].x;
			}
/*
	output[dtid.x].x = clamp(output[dtid.x].x, -1.,1.);	
	output[dtid.x].y = clamp(output[dtid.x].y, -1.,1.);	
	output[dtid.x].z = clamp(output[dtid.x].z, -1.,1.);	
*/	
	output[dtid.x] = clamp(output[dtid.x], MinMax.x,MinMax.y);	
	
	
	
	
	
}

technique11 GroundClipX
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_maxX() ) );
	}
}


technique11 GroundClipY
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_maxY() ) );
	}
}

technique11 GroundClipZ
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_maxZ() ) );
	}
}

technique11 bBox
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_bBox() ) );
	}
}








