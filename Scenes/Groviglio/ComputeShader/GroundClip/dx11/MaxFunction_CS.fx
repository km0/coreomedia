//@author: Noir
//@tags: ground clip ,max function
//@credits: NSYNK


RWStructuredBuffer<float3> output : BACKBUFFER;

StructuredBuffer<float3> bufferIN;
uint threadCount;

float ClipPlan = 0.0;

[numthreads(128,1,1)]
void CS_maxX( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];
	output[dtid.x].x = max(output[dtid.x].x, ClipPlan);	
	
}

[numthreads(128,1,1)]
void CS_maxY( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];
	output[dtid.x].y = max(output[dtid.x].y, ClipPlan);	
	
}

[numthreads(128,1,1)]
void CS_maxZ( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }
	
	output[dtid.x] = bufferIN[dtid.x];
	output[dtid.x].z = max(output[dtid.x].z, ClipPlan);	
	
}

technique11 GroundClipX
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_maxX() ) );
	}
}


technique11 GroundClipY
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_maxY() ) );
	}
}

technique11 GroundClipZ
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CS_maxZ() ) );
	}
}





