
RWStructuredBuffer<float3> output : BACKBUFFER;

float lerpValue = 0;


StructuredBuffer<float> lerpValueBuffer;

StructuredBuffer<float3> in1, in2, in3, in4;


uint threadCount;

[numthreads(128,1,1)]
void CSlerp( uint3 dtid : SV_DispatchThreadID)
{ 

if (dtid.x >= threadCount) { return; }

	float factor = lerpValue%1;
	
if (lerpValue < 1)
	output[dtid.x] = lerp(in1[dtid.x], in2[dtid.x], factor);

else if (lerpValue < 2 && lerpValue >= 1)
	output[dtid.x] = lerp(in2[dtid.x], in3[dtid.x], factor);

else if (lerpValue < 3 && lerpValue >= 2)
	output[dtid.x] = lerp(in3[dtid.x], in4[dtid.x], factor);

	
	output[dtid.x].y = max(output[dtid.x].y, 0);	
	
}



technique11 Lerp
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSlerp() ) );
	}
}







