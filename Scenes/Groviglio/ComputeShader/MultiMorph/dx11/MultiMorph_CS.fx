
RWStructuredBuffer<float3> output : BACKBUFFER;

float lerpValue = 0;
//float GroundClip = 0.0f;

StructuredBuffer<float> lerpValueBuffer;

StructuredBuffer<float3> in1, in2, in3, in4, in5 ,in6;


uint threadCount;

[numthreads(128,1,1)]
void CSlerp( uint3 dtid : SV_DispatchThreadID)
{ 

if (dtid.x >= threadCount) { return; }

//	float GroundClip = in1[dtid.x];
//	if (GroundClip >GroundClip){ 
	
		
	float factor = lerpValue%1;
	
if (lerpValue < 1)
	output[dtid.x] = lerp(in1[dtid.x], in2[dtid.x], factor);

else if (lerpValue < 2 && lerpValue >= 1)
	output[dtid.x] = lerp(in2[dtid.x], in3[dtid.x], factor);

else if (lerpValue < 3 && lerpValue >= 2)
	output[dtid.x] = lerp(in3[dtid.x], in4[dtid.x], factor);

else if (lerpValue < 4 && lerpValue >= 3)
	output[dtid.x] = lerp(in4[dtid.x], in5[dtid.x], factor);
	
else if (lerpValue < 5 && lerpValue >= 4)
	output[dtid.x] = lerp(in5[dtid.x], in6[dtid.x], factor);
	//}
	
}

[numthreads(128,1,1)]
void CSlerpBuffered( uint3 dtid : SV_DispatchThreadID)
{ 
	if (dtid.x >= threadCount) { return; }

float factor = lerpValueBuffer[dtid.x%50]%1;
float sector = lerpValueBuffer[dtid.x%50];
	
if (sector < 1)
	output[dtid.x] = lerp(in1[dtid.x], in2[dtid.x], factor);

else if (sector < 2 && lerpValue >= 1)
	output[dtid.x] = lerp(in2[dtid.x], in3[dtid.x], factor);

else if (sector < 3 && lerpValue >= 2)
	output[dtid.x] = lerp(in3[dtid.x], in4[dtid.x], factor);

else if (sector < 4 && lerpValue >= 3)
	output[dtid.x] = lerp(in4[dtid.x], in5[dtid.x], factor);
	
else if (sector < 5 && lerpValue >= 4)
	output[dtid.x] = lerp(in5[dtid.x], in6[dtid.x], factor);
	
output[dtid.x].y = max(output[dtid.x].y, 0);	
	
}




technique11 Lerp
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSlerp() ) );
	}
}





technique11 LerpBuffered
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSlerpBuffered() ) );
	}
}






