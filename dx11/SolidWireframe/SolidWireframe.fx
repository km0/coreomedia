//@author: dottore
//@help: display the wireframe with width control and diagonal edge settings
//@tags: wireframe, triangle, quad, diagonal, width
//@credits: nVidia

//----------------------------------------------------------------------------------
// File:   SolidWireframe.fx
// Author: Samuel Gateau
// Email:  sdkfeedback@nvidia.com
// 
// Copyright (c) 2007 NVIDIA Corporation. All rights reserved.
//
// TO  THE MAXIMUM  EXTENT PERMITTED  BY APPLICABLE  LAW, THIS SOFTWARE  IS PROVIDED
// *AS IS*  AND NVIDIA AND  ITS SUPPLIERS DISCLAIM  ALL WARRANTIES,  EITHER  EXPRESS
// OR IMPLIED, INCLUDING, BUT NOT LIMITED  TO, IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE.  IN NO EVENT SHALL  NVIDIA OR ITS SUPPLIERS
// BE  LIABLE  FOR  ANY  SPECIAL,  INCIDENTAL,  INDIRECT,  OR  CONSEQUENTIAL DAMAGES
// WHATSOEVER (INCLUDING, WITHOUT LIMITATION,  DAMAGES FOR LOSS OF BUSINESS PROFITS,
// BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, OR ANY OTHER PECUNIARY LOSS)
// ARISING OUT OF THE  USE OF OR INABILITY  TO USE THIS SOFTWARE, EVEN IF NVIDIA HAS
// BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//
//
//----------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------
// Constant Buffer Variables
//--------------------------------------------------------------------------------------

float4x4 tWV : WORLDVIEW ;
float4x4 tWVP : WORLDVIEWPROJECTION ;

float2 res : TARGETSIZE ;

float LineWidth <String uiname="Line Width";> = 1.5;

bool customDiagonal <String uiname="Customize Diagonal";> = 1;
float DiagonalWidthMult <String uiname="Diagonal Width Mult";> = 1;
bool diagonalPattern <String uiname="Diagonal Pattern Enable";> = 1;
float PatternPeriod <String uiname="Diagonal Pattern Period";> = 1.5;

bool fadeEnable <String uiname="Depth Fade Enable";>;
float FadeDistance <String uiname="Depth Fade Distance";> = 50;

float4 FillColor <bool color=true;String uiname="Fill Color";> = { 0.2, 0.2, 0.2, 1 };
float4 WireColor <bool color=true;String uiname="Wire Color";> = { 1, 1, 1, 1 };
float4 PatternColor  <bool color=true;String uiname="Diagonal Color";> = { 1, 1, 0.5, 1 };

uint infoA[]     = { 0, 0, 0, 0, 1, 1, 2 };
uint infoB[]     = { 1, 1, 2, 0, 2, 1, 2 };
uint infoAd[]    = { 2, 2, 1, 1, 0, 0, 0 };
uint infoBd[]    = { 2, 2, 1, 2, 0, 2, 1 }; 
uint infoEdge0[] = { 0, 2, 0, 0, 0, 0, 2 }; 

// =============================================================================
// UTILS FUNCTIONS =============================================================
// =============================================================================

// From window pixel pos to projection frame at the specified z (view frame). 
float2 projToWindow(in float4 pos)
{
    //return float2(  Viewport.x*0.5*((pos.x/pos.w) + 1) + Viewport.z,
    //                Viewport.y*0.5*(1-(pos.y/pos.w)) + Viewport.w );
    return float2(  res.x*0.5*((pos.x/pos.w) + 1) ,
                    res.y*0.5*(1-(pos.y/pos.w)));
}


// Compute the triangle face normal from 3 points
float3 faceNormal(in float3 posA, in float3 posB, in float3 posC)
{
    return normalize( cross(normalize(posB - posA), normalize(posC - posA)) );
}

// =============================================================================
// VERTEX SHADERS ==============================================================
// =============================================================================

struct VS_INPUT
{
    float4 Pos  : POSITION ;
    //float4 Nor  : NORMAL ;
    float2 Tex  : TEXCOORD0 ;
};

struct PS_OUT
{
    float4 Pos  : POSITION ;
    float4 PosV : TEXCOORD0 ;
    float3 PosW : TEXCOORD1 ;
};

PS_OUT VS( VS_INPUT In )
{
    PS_OUT Out;
    Out.Pos = mul(float4(In.Pos.xyz, 1), tWVP);
    Out.PosV = mul(float4(In.Pos.xyz, 1), tWV);
	Out.PosW = In.Pos.xyz;
    return Out;
}

// =============================================================================
// GEOMETRY SHADERS ============================================================
// =============================================================================

struct GS_OUT
{
    float4 Pos : SV_POSITION ;
    float4 Col : TEXCOORD0 ;
    noperspective float4 EdgeA: TEXCOORD1 ;
    noperspective float4 EdgeB: TEXCOORD2 ;
    uint Case : TEXCOORD3 ;
};

[maxvertexcount(3)]
void GSSolidWire( triangle PS_OUT input[3], inout TriangleStream<GS_OUT> outStream )
{
    GS_OUT Out;

	//Grab triangles positions
	float3 t1 = input[0].PosW;
    float3 t2 = input[1].PosW;
	float3 t3 = input[2].PosW;

	//Compute edge length
	float dl1 = length(t2 - t1);
	float dl2 = length(t3 - t2);
	float dl3 = length(t3 - t1);
	
	//Get max length
	float maxdistsqr = max(max(dl1,dl2),dl3);
	
	uint diagonalIndex = 0;
	if(dl1 == maxdistsqr) diagonalIndex = 0;
	else if(dl2 == maxdistsqr) diagonalIndex = 1;
	else if(dl3 == maxdistsqr) diagonalIndex = 2;
	
	PS_OUT In[3] = input;
	
	if(diagonalIndex == 1)
	{
		In[0] = input[1];
		In[1] = input[2];
		In[2] = input[0];
	}
	else if(diagonalIndex == 2)
	{
		In[0] = input[2];
		In[1] = input[0];
		In[2] = input[1];
	}
	
    // Compute the case from the positions of point in space.
    Out.Case = (In[0].Pos.z < 0)*4 + (In[1].Pos.z < 0)*2 + (In[2].Pos.z < 0); 

    // If case is all vertices behind viewpoint (case = 7) then cull.
    if (Out.Case == 7) return;

    // Shade and colour face just for the "all in one" technique.
    Out.Col = 1;//shadeFace(In[0].PosV, In[1].PosV, In[2].PosV);

   // Transform position to window space
    float2 points[3];
    points[0] = projToWindow(In[0].Pos);
    points[1] = projToWindow(In[1].Pos);
    points[2] = projToWindow(In[2].Pos);

    // If Case is 0, all projected points are defined, do the
    // general case computation
    if (Out.Case == 0) 
    {
        Out.EdgeA = float4(0,0,0,0);
        Out.EdgeB = float4(0,0,0,0);

        // Compute the edges vectors of the transformed triangle
        float2 edges[3];
        edges[0] = points[1] - points[0];
        edges[1] = points[2] - points[1];
        edges[2] = points[0] - points[2];

        // Store the length of the edges
        float lengths[3];
        lengths[0] = length(edges[0]);
        lengths[1] = length(edges[1]);
        lengths[2] = length(edges[2]);

        // Compute the cos angle of each vertices
        float cosAngles[3];
        cosAngles[0] = dot( -edges[2], edges[0]) / ( lengths[2] * lengths[0] );
        cosAngles[1] = dot( -edges[0], edges[1]) / ( lengths[0] * lengths[1] );
        cosAngles[2] = dot( -edges[1], edges[2]) / ( lengths[1] * lengths[2] );

        // The height for each vertices of the triangle
        float heights[3];
        heights[1] = lengths[0]*sqrt(1 - cosAngles[0]*cosAngles[0]);
        heights[2] = lengths[1]*sqrt(1 - cosAngles[1]*cosAngles[1]);
        heights[0] = lengths[2]*sqrt(1 - cosAngles[2]*cosAngles[2]);

        float edgeSigns[3];
        edgeSigns[0] = (edges[0].x > 0 ? 1 : -1);
        edgeSigns[1] = (edges[1].x > 0 ? 1 : -1);
        edgeSigns[2] = (edges[2].x > 0 ? 1 : -1);

        float edgeOffsets[3];
        edgeOffsets[0] = lengths[0]*(0.5 - 0.5*edgeSigns[0]);
        edgeOffsets[1] = lengths[1]*(0.5 - 0.5*edgeSigns[1]);
        edgeOffsets[2] = lengths[2]*(0.5 - 0.5*edgeSigns[2]);

        Out.Pos =( In[0].Pos );
        Out.EdgeA[0] = 0;
        Out.EdgeA[1] = heights[0];
        Out.EdgeA[2] = 0;
        Out.EdgeB[0] = edgeOffsets[0];
        Out.EdgeB[1] = edgeOffsets[1] + edgeSigns[1] * cosAngles[1]*lengths[0];
        Out.EdgeB[2] = edgeOffsets[2] + edgeSigns[2] * lengths[2];
        outStream.Append( Out );

        Out.Pos = ( In[1].Pos );
        Out.EdgeA[0] = 0;
        Out.EdgeA[1] = 0;
        Out.EdgeA[2] = heights[1];
        Out.EdgeB[0] = edgeOffsets[0] + edgeSigns[0] * lengths[0];
        Out.EdgeB[1] = edgeOffsets[1];
        Out.EdgeB[2] = edgeOffsets[2] + edgeSigns[2] * cosAngles[2]*lengths[1];
        outStream.Append( Out );

        Out.Pos = ( In[2].Pos );
        Out.EdgeA[0] = heights[2];
        Out.EdgeA[1] = 0;
        Out.EdgeA[2] = 0;
        Out.EdgeB[0] = edgeOffsets[0] + edgeSigns[0] * cosAngles[0]*lengths[2];
        Out.EdgeB[1] = edgeOffsets[1] + edgeSigns[1] * lengths[1];
        Out.EdgeB[2] = edgeOffsets[2];
        outStream.Append( Out );

        outStream.RestartStrip();
    }
    // Else need some tricky computations
    else
    {
        // Then compute and pass the edge definitions from the case
        Out.EdgeA.xy = points[ infoA[Out.Case] ];
        Out.EdgeB.xy = points[ infoB[Out.Case] ];

		Out.EdgeA.zw = normalize( Out.EdgeA.xy - points[ infoAd[Out.Case] ] ); 
        Out.EdgeB.zw = normalize( Out.EdgeB.xy - points[ infoBd[Out.Case] ] );
		
		// Generate vertices
        Out.Pos =( In[0].Pos );
        outStream.Append( Out );
     
        Out.Pos = ( In[1].Pos );
        outStream.Append( Out );

        Out.Pos = ( In[2].Pos );
        outStream.Append( Out );

        outStream.RestartStrip();
    }
}

// =============================================================================
// PIXEL SHADERS ===============================================================
// =============================================================================

// FUNCTIONS ===================================================================

float evalMinDistanceToEdges(in GS_OUT input)
{
    float dist;

    // The easy case, the 3 distances of the fragment to the 3 edges is already
    // computed, get the min.
    if (input.Case == 0)
    {
        dist = min ( min (input.EdgeA.x, input.EdgeA.y), input.EdgeA.z);
    }
    // The tricky case, compute the distances and get the min from the 2D lines
    // given from the geometry shader.
    else
    {
        // Compute and compare the sqDist, do one sqrt in the end.
        	
        float2 AF = input.Pos.xy - input.EdgeA.xy;
        float sqAF = dot(AF,AF);
        float AFcosA = dot(AF, input.EdgeA.zw);
        dist = abs(sqAF - AFcosA*AFcosA);

        float2 BF = input.Pos.xy - input.EdgeB.xy;
        float sqBF = dot(BF,BF);
        float BFcosB = dot(BF, input.EdgeB.zw);
        dist = min( dist, abs(sqBF - BFcosB*BFcosB) );
       
        // Only need to care about the 3rd edge for some cases.
        if (input.Case == 1 || input.Case == 2 || input.Case == 4)
        {
            float AFcosA0 = dot(AF, normalize(input.EdgeB.xy - input.EdgeA.xy));
			dist = min( dist, abs(sqAF - AFcosA0*AFcosA0) );
	    }

        dist = sqrt(dist);
    }

    return dist;
}

float evalMinDistanceToEdgesExt(  in GS_OUT input, 
                                  out float3 edgeSqDists, 
                                  out float3 edgeCoords, 
                                  out uint3 edgeOrder)
{
    // The easy case, the 3 distances of the fragment to the 3 edges is already
    // computed, get the min.
    if (input.Case == 0)
    {
        edgeSqDists = input.EdgeA.xyz * input.EdgeA.xyz;
        edgeCoords = input.EdgeB.xyz;
        edgeOrder = uint3(0, 1, 2);

        if (edgeSqDists[1] < edgeSqDists[0])
        {
            edgeOrder.xy = edgeOrder.yx;
        }
        if (edgeSqDists[2] < edgeSqDists[edgeOrder.y])
        {
            edgeOrder.yz = edgeOrder.zy;
        }
        if (edgeSqDists[2] < edgeSqDists[edgeOrder.x])
        {
            edgeOrder.xy = edgeOrder.yx;
        }
    }
    // The tricky case, compute the distances and get the min from the 2D lines
    // given from the geometry shader.
    else
    {
        float2 AF = input.Pos.xy - input.EdgeA.xy;
        float sqAF = dot(AF,AF);
        float AFcosA = dot(AF, input.EdgeA.zw);
        edgeSqDists[0] = abs(sqAF - AFcosA*AFcosA);
        edgeOrder = uint3(0, 1, 2);
        edgeCoords[0] = abs(AFcosA);

        float2 BF = input.Pos.xy - input.EdgeB.xy;
        float sqBF = dot(BF,BF);
        float BFcosB = dot(BF, input.EdgeB.zw);
        edgeSqDists[1] = abs(sqBF - BFcosB*BFcosB);
        edgeCoords[1] = abs(BFcosB);

        if (edgeSqDists[1] < edgeSqDists[0])
        {
            edgeOrder.xy = edgeOrder.yx;
        }

        if (input.Case == 1 || input.Case == 2 || input.Case == 4)
        {
            float AFcosA0 = dot(AF, normalize(input.EdgeB.xy - input.EdgeA.xy));         
            edgeSqDists[2] = abs(sqAF - AFcosA0*AFcosA0);
            edgeCoords[2] = abs(AFcosA0);

            if (edgeSqDists[2] < edgeSqDists[edgeOrder.y])
            {
                edgeOrder.yz = edgeOrder.zy;
            }
            
            if (edgeSqDists[2] < edgeSqDists[edgeOrder.x])
            {
                edgeOrder.xy = edgeOrder.yx;
            }
        }
        else
        {
			edgeSqDists[2] = 0;
			edgeCoords[2] = 0;
		}
    }

    return sqrt(edgeSqDists[edgeOrder.x]);
}

// PIXEL SHADERS ===============================================================

float4 PSSolidWirePattern( GS_OUT input) : SV_Target
{
    // Standard wire color
    float4 color = WireColor;
	float dist = 0;
	if(customDiagonal)
	{
		// Compute the shortest square distance between the fragment and the edges.
	    float3 edgeSqDists;
	    float3 edgeCoords;
	    uint3 edgeOrder;
	    dist = evalMinDistanceToEdgesExt(input, edgeSqDists, edgeCoords, edgeOrder);
	    float realLineWidth = 0.5*LineWidth;
	    // Except if on the diagonal edge
	    if ( infoEdge0[input.Case] == edgeOrder.x )
	    {
	        if (dist > LineWidth*0.5*DiagonalWidthMult+1) discard;  	 	
	        float patternPos = 0; //1;
	    	if(diagonalPattern) patternPos=(abs(edgeCoords[edgeOrder.x]) % (PatternPeriod*LineWidth*DiagonalWidthMult)) - LineWidth*DiagonalWidthMult*0.5;
	        dist = (patternPos*patternPos + dist*dist);
	        color = PatternColor;
	        realLineWidth = LineWidth * DiagonalWidthMult * 0.5;
	         // Filling the corners near the vertices with the WireColor
	        if ( edgeSqDists[edgeOrder.y] < pow(0.5*LineWidth+1, 2) )
	        {
	            dist = edgeSqDists[edgeOrder.y];
	            color = WireColor;
	            realLineWidth = LineWidth * 0.5;
	        }
			else if(DiagonalWidthMult == 0) discard;   	
	        dist = sqrt(dist); 	
	    }
		// Cull fragments too far from the edge.
	    else if (dist > 0.5*LineWidth+1) discard;	
	    // Map the computed distance to the [0,2] range on the border of the line.
	    dist = clamp((dist - (realLineWidth - 1)), 0, 2);
	}
	
	else
	{
	    // Compute the shortest distance between the fragment and the edges.
	    dist = evalMinDistanceToEdges(input);
	    // Cull fragments too far from the edge.
	    if (dist > 0.5*LineWidth+1) discard;		
	    // Map the computed distance to the [0,2] range on the border of the line.
		dist = clamp((dist - (0.5*LineWidth - 1)), 0, 2);
	}
    // Alpha is computed from the function exp2(-2(x)^2).
    dist *= dist;
    float alpha = exp2(-2*dist);
	// faded by distance
    // Dividing by pos.w, the depth in view space
	if(fadeEnable) alpha *= clamp(FadeDistance / input.Pos.w, 0, 1);
	color.a *= alpha;
    return color;
}

float4 PSSolidWirePatternFill( GS_OUT input) : SV_Target
{
    // Standard wire color
    float4 color = WireColor;
	float dist = 0;
	if(customDiagonal)
	{
		// Compute the shortest square distance between the fragment and the edges.
	    float3 edgeSqDists;
	    float3 edgeCoords;
	    uint3 edgeOrder;
	    dist = evalMinDistanceToEdgesExt(input, edgeSqDists, edgeCoords, edgeOrder);
	    float realLineWidth = 0.5*LineWidth;
	    // Except if on the diagonal edge
	    if ( infoEdge0[input.Case] == edgeOrder.x )
	    {
	        float patternPos = 0; //1;
	    	if(diagonalPattern) patternPos=(abs(edgeCoords[edgeOrder.x]) % (PatternPeriod*LineWidth*DiagonalWidthMult)) - LineWidth*DiagonalWidthMult*0.5;
	        dist = (patternPos*patternPos + dist*dist);
	        color = PatternColor;
	        realLineWidth = LineWidth * DiagonalWidthMult * 0.5;
	         // Filling the corners near the vertices with the WireColor
	        if ( edgeSqDists[edgeOrder.y] < pow(0.5*LineWidth+1, 2) )
	        {
	            dist = edgeSqDists[edgeOrder.y];
	            color = WireColor;
	            realLineWidth = LineWidth * 0.5;
	        }
			//else if(DiagonalWidthMult == 0) discard;   	
	        dist = sqrt(dist); 	
	    }
	    // Map the computed distance to the [0,2] range on the border of the line.
	    dist = clamp((dist - (realLineWidth - 1)), 0, 2);
	}
	
	else
	{
	    // Compute the shortest distance between the fragment and the edges.
	    dist = evalMinDistanceToEdges(input);
	    // Map the computed distance to the [0,2] range on the border of the line.
		dist = clamp((dist - (0.5*LineWidth - 1)), 0, 2);
	}
    // Alpha is computed from the function exp2(-2(x)^2).
    dist *= dist;
    float alpha = exp2(-2*dist);
	// faded by distance
    // Dividing by pos.w, the depth in view space
	if(fadeEnable) alpha *= clamp(FadeDistance / input.Pos.w, 0, 1);

	color = lerp(FillColor, color, alpha);
	return color;
}

// =============================================================================
// TECHNIQUES ==================================================================
// =============================================================================

DepthStencilState DSSDepthLessEqual
{
  DepthEnable = true;
  DepthWriteMask = 0x00;
  DepthFunc = Less_Equal;
};

BlendState BSBlending
{
    BlendEnable[0] = TRUE;
    SrcBlend = SRC_ALPHA;
    DestBlend = INV_SRC_ALPHA ;
    BlendOp = ADD;
    SrcBlendAlpha = SRC_ALPHA;
    DestBlendAlpha = DEST_ALPHA;
    BlendOpAlpha = ADD;
    RenderTargetWriteMask[0] = 0x0F;
};


technique10 SolidWire
    <string info = "SolidWirePattern ";>
{
    pass
    {
        SetDepthStencilState( DSSDepthLessEqual, 0 );
        SetBlendState( BSBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( CompileShader( gs_4_0, GSSolidWire() ) );
        SetPixelShader( CompileShader( ps_4_0, PSSolidWirePattern() ) );
    }
}

technique10 SolidWireFill
    <string info = "SolidWirePattern with fill color";>
{
    pass
    {
        //SetDepthStencilState( DSSDepthLessEqual, 0 );
        //SetBlendState( BSBlending, float4( 0.0f, 0.0f, 0.0f, 0.0f ), 0xFFFFFFFF );
        SetVertexShader( CompileShader( vs_4_0, VS() ) );
        SetGeometryShader( CompileShader( gs_4_0, GSSolidWire() ) );
        SetPixelShader( CompileShader( ps_4_0, PSSolidWirePatternFill() ) );
    }
}


