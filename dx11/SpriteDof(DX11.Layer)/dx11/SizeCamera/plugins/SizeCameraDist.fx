
RWStructuredBuffer<float> output : BACKBUFFER;

float3 CamPos;

StructuredBuffer<float3> Pos3d,cam;
float factor = 0.5;

// Constants (tweakable):
float minPointScale = 0.1;
float maxPointScale = 0.7;
float maxDistance   = 100.0;
float gamma = .45;
uint threadCount;

[numthreads(128,1,1)]
void CSsize( uint3 dtid : SV_DispatchThreadID)
{ 

if (dtid.x >= threadCount) { return; }
	
	float dist = pow((length(Pos3d[dtid.x] - CamPos)),gamma);
	
	float pointScale =  (dist / maxDistance);
    pointScale = max(pointScale, minPointScale);
  	pointScale = min(pointScale, maxPointScale);
	output[dtid.x] = pow(pointScale,factor);
	//output[dtid.x] =  pointScale;
	
	
	
}



technique11 DistSize
{
	pass P0
	{
		SetComputeShader( CompileShader( cs_5_0, CSsize() ) );
	}
}







